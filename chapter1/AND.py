def and_gate(x,y):
  if x == y:
    return 1
  return 0

def or_gate(x, y):
  return x or y

def not_gate(x):
  if x == 1:
    return 0
  return 1


def xor_gate(x, y):
  return or_gate(and_gate(x,not_gate(y)), and_gate(not_gate(x), y))



def complex_and(*args):
  print(args)
  temp, rest, i = args[0], args[1:], 0
  while i < len(rest):
    temp = and_gate(temp, rest[i])
    i += 1
  
  return temp

print(xor_gate(0,0))
print(xor_gate(0,1))
print(xor_gate(1,0))
print(xor_gate(1,1))